const request = require('supertest')
const app = require('../app')

describe('User History API', () => {
    it('should show all user histories', async () => {
        const res = await request(app)
            .get('/api/histories')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('data');
    });

    it('should show user history with limitation using pagination', async () => {
        const res = await request(app)
            .get('/api/histories')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
            .query({
                page: 2,
                row: 2
            })
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('data');
    });

    it('should throw authentication failed', async () => {
        const res = await request(app)
            .get('/api/histories')
        expect(res.statusCode).toEqual(403);
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toEqual('Unauthenticated');
    });

    it('should show a user history', async () => {
        const res = await request(app)
            .get('/api/histories/2')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('data');
    });

    it('should throw error user history not found', async () => {
        const res = await request(app)
            .get('/api/histories/200')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
        expect(res.statusCode).toEqual(404);
        expect(res.body).toHaveProperty('message');
    });

    it('should create a new user history', async () => {
        const res = await request(app)
            .post('/api/histories')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
            .set('Content-Type', 'application/json')
            .send({
                user_id: 3,
                player_name: 'test name',
                level: 20
            })
        expect(res.statusCode).toEqual(201);
        expect(res.body).toHaveProperty('data');
    })

    it('should update a user history', async () => {
        const res = await request(app)
            .post('/api/histories/3')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
            .set('Content-Type', 'application/json')
            .send({
                user_id: 3,
                player_name: 'testt name',
                level: 20
            })
        expect(res.statusCode).toEqual(201);
        expect(res.body).toHaveProperty('data');
    })

    it('should throw update error user history not found', async () => {
        const res = await request(app)
            .post('/api/histories/500')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
            .set('Content-Type', 'application/json')
            .send({
                user_id: 3,
                player_name: 'testt name',
                level: 20
            })
        expect(res.statusCode).toEqual(404);
        expect(res.body).toHaveProperty('message');
    });

    it('should delete a user history', async () => {
        const res = await request(app)
            .del('/api/histories/3')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('data')
    })

    it('should throw delete error user history not found', async () => {
        const res = await request(app)
            .del('/api/histories/300')
            .set('email', "admin@gmail.com")
            .set('password', 12345678)
        expect(res.statusCode).toEqual(404)
        expect(res.body).toHaveProperty('message')
    });
});