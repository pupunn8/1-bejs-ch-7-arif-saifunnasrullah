const authentication = (req, res, next) => {
    console.log(req.headers);
    const {email, password} = req.headers;
    if(!(email == "admin@gmail.com" && password == 12345678)) {
        return res.status(403).json({
            status: "Unauthenticated",
            message: "Anda tidak diperkenankan mengakses link ini"
        });
    };
    next();
}

module.exports = {
    authentication
};